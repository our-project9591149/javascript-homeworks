"use strict";

/*1. В чому полягає відмінність localStorage і sessionStorage?
Дані, збережені в sessionStorage, доступні лише для тієї самої вкладки.
Дані, збережені в localStorage, доступні для всіх вікон.


2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?


3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
Вони очищаються.
*/

/*Практичне завдання:


Реалізувати можливість зміни колірної теми користувача.


Технічні вимоги:


- Взяти готове домашнє завдання HW-5 "Book Shop" з блоку Basic HMTL/CSS.

- Додати на макеті кнопку "Змінити тему".

- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.

- Вибрана тема повинна зберігатися після перезавантаження сторінки.


Примітки: 

- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;

- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою. */
function changingTheme() {
    let currentTheme = localStorage.getItem("theme");
    const section = document.querySelector(".");
    if (currentTheme === "dark") {
        document.body.style.backgroundColor = "#f2f2f2";
        localStorage.setItem("theme", "light");
    } else {
        document.body.style.backgroundColor = "#333";
        localStorage.setItem("theme", "dark");
    }
}
document.getElementById("themeId").addEventListener("click", changingTheme);
