"use strict";
/*1. В чому відмінність між setInterval та setTimeout?
Метод setTimeout дозволяє запускати функцію один раз через певний інтервал часу.
Метод setInterval дозволяє запускати функцію багаторазово через
заданий проміжок часу, починаючи з певного інтервалу.

2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?
Ні.

3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
Використати clearInterval.
*/

/*-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди.
 Новий текст повинен вказувати, що операція виконана успішно. */
function changeText() {
    let div = document.getElementById("div");

    setTimeout(function () {
        div.textContent = "Operation is completed!";
    }, 3000);
}
/*Практичне завдання 2:
Реалізуйте таймер зворотного відліку, використовуючи setInterval.
 При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div.
 Після досягнення 1 змініть текст на "Зворотній відлік завершено".*/
let maxNumber = 10;
let timerDisplay = document.getElementById("timer");
let timer = setInterval(function () {
    maxNumber--;
    timerDisplay.textContent = "Відлік: " + maxNumber;
    if (maxNumber === 0) {
        clearInterval(timer);
        timerDisplay.textContent = "Відлік завершено";
    }
}, 1000);
