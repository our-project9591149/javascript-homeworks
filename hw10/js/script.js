"use strict";
/*1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
Для створення DOM-елементів існують два методи:
1) document.createElement(tag)
2) document.createTextNode(text)
Для додавання елементу ми можемо використовувати метод: elem.insertAdjacentHTML(where, html).
У цього методу є два аналоги:
1) elem.insertAdjacentText(where, text) – та ж сама синтаксис, але рядок
тексту вставляється "як текст", а не HTML,
2) elem.insertAdjacentElement(where, elem) – той же самий синтаксис, але
вставляє елемент.

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
let navigationElement = document.querySelector('.navigation');
navigationElement.remove();

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
елемент.before(), елемент.after(), елемент.pretend(), елемент.append().
*/

/*1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". 
Додайте цей елемент в footer після параграфу.*/
let learnMoreLink = document.createElement("a");
learnMoreLink.textContent = "Learn More";
learnMoreLink.setAttribute("href", "#");
document.querySelector("footer").appendChild(learnMoreLink);
/*2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу. */
let ratingSelect = document.createElement("select");
ratingSelect.setAttribute("id", "rating");
document
    .querySelector("main")
    .insertBefore(ratingSelect, document.querySelector(".features"));
