"use strict";
//Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
//! Це виконання частини коду паралельно з виконанням коду основної програми.
/*Завдання
Написати програму "Я тебе знайду по IP"

Технічні вимоги:

    Створити просту HTML-сторінку з кнопкою Знайти по IP.
    Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
    Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
    під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
    Усі запити на сервер необхідно виконати за допомогою async await.

Примітка
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.*/
document
    .getElementById("findIpButton")
    .addEventListener("click", async function () {
        const resultDiv = document.getElementById("result");
        resultDiv.innerHTML = "Завантаження...";

        try {
            const ipResponse = await fetch(
                "https://api.ipify.org/?format=json"
            );
            const ipData = await ipResponse.json();
            const ipAddress = ipData.ip;

            const addressResponse = await fetch(
                `http://ip-api.com/json/${ipAddress}`
            );
            const addressData = await addressResponse.json();
            console.log(addressData);

            resultDiv.innerHTML = `
            <h2>Інформація про IP-адресу:</h2>
            <p><strong>IP Адреса:</strong> ${ipAddress}</p>
            <p><strong>Континент:</strong> ${addressData.timezone}</p>
            <p><strong>Країна:</strong> ${addressData.country}</p>
            <p><strong>Регіон:</strong> ${addressData.region}</p>
            <p><strong>Місто:</strong> ${addressData.city}</p>
            <p><strong>Район:</strong> ${addressData.regionName}</p>
        `;
        } catch (error) {
            console.error("Error:", error);
            resultDiv.innerHTML = "Сталася помилка. Спробуйте ще раз.";
        }
    });
