"use strict";
/*1. Опишіть своїми словами що таке Document Object Model (DOM)
Це структурована модель побудови коду.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerHTML для встановлення вмісту включає розбір та відображення HTML.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
За допомогою методів:
1)getElement(s)by
2)querySelector


4. Яка різниця між nodeList та HTMLCollection?

*/

/*1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
Використайте 2 способи для пошуку елементів.
Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).*/
let features1 = document.querySelectorAll(".feature");
console.log("Знайдені елементи (спосіб 1):", features1);
features1.forEach(function (feature) {
    feature.style.textAlign = "center";
});

// let features2 = document.getElementsByClassName('feature');
// console.log("Знайдені елементи (спосіб 2):", features2);
// for (let i = 0; i < features2.length; i++) {
//     features2[i].style.textAlign = 'center';
// }

/*2. Змініть текст усіх елементів h2 на "Awesome feature".*/
let h2Text = document.getElementsByTagName("h2");
for (let i = 0; i < h2Text.length; i++) {
    h2Text[i].textContent = "Awesome feature";
}
/*3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".*/
let featureTitles = document.querySelectorAll(".feature-title");
featureTitles.forEach(function (title) {
    title.textContent += "!";
});
