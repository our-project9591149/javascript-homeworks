"use strict";

const DATA = [
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m1.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "8 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f1.png",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m2.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Тетяна",
    "last name": "Мороз",
    photo: "./img/trainers/trainer-f2.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
  },
  {
    "first name": "Сергій",
    "last name": "Коваленко",
    photo: "./img/trainers/trainer-m3.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
  },
  {
    "first name": "Олена",
    "last name": "Лисенко",
    photo: "./img/trainers/trainer-f3.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
  },
  {
    "first name": "Андрій",
    "last name": "Волков",
    photo: "./img/trainers/trainer-m4.jpg",
    specialization: "Бійцівський клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
  },
  {
    "first name": "Наталія",
    "last name": "Романенко",
    photo: "./img/trainers/trainer-f4.jpg",
    specialization: "Дитячий клуб",
    category: "спеціаліст",
    experience: "3 роки",
    description:
      "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
  },
  {
    "first name": "Віталій",
    "last name": "Козлов",
    photo: "./img/trainers/trainer-m5.jpg",
    specialization: "Тренажерний зал",
    category: "майстер",
    experience: "10 років",
    description:
      "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
  },
  {
    "first name": "Юлія",
    "last name": "Кравченко",
    photo: "./img/trainers/trainer-f5.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
  },
  {
    "first name": "Олег",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-m6.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "12 років",
    description:
      "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
  },
  {
    "first name": "Лідія",
    "last name": "Попова",
    photo: "./img/trainers/trainer-f6.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
  },
  {
    "first name": "Роман",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m7.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
  },
  {
    "first name": "Анастасія",
    "last name": "Гончарова",
    photo: "./img/trainers/trainer-f7.jpg",
    specialization: "Басейн",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
  },
  {
    "first name": "Валентин",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-m8.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
  },
  {
    "first name": "Лариса",
    "last name": "Петренко",
    photo: "./img/trainers/trainer-f8.jpg",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "7 років",
    description:
      "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
  },
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m9.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "11 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f9.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m10.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Наталія",
    "last name": "Бондаренко",
    photo: "./img/trainers/trainer-f10.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "8 років",
    description:
      "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
  },
  {
    "first name": "Андрій",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m11.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
  },
  {
    "first name": "Софія",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-f11.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "6 років",
    description:
      "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
  },
  {
    "first name": "Дмитро",
    "last name": "Ковальчук",
    photo: "./img/trainers/trainer-m12.png",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
  },
  {
    "first name": "Олена",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-f12.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "5 років",
    description:
      "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
  },
];

let cardsContainer = document.querySelector(".trainers-cards__container");
let cardTrainer = document.querySelector("#trainer-card");
let trainerLi = document.querySelector(".trainer");

let fragment = document.createDocumentFragment();

createCards(DATA);

function createCard(elem) {
  fragment = cardTrainer.content.cloneNode(true);
  let TName = elem["first name"];
  let TSurname = elem["last name"];
  fragment.querySelector(".trainer__name").textContent = TSurname + " " + TName;
  let Timage = elem.photo;
  fragment.querySelector(".trainer__img").src = Timage;
  fragment.querySelector(".trainer").setAttribute("data-exp", elem.experience);
  cardsContainer.append(fragment);
}

function createCards(array) {
  array.forEach((elem) => {
    createCard(elem);
  });
}

let sidebar = document.querySelector(".sidebar");
sidebar.removeAttribute("hidden");

let sorting = document.querySelector(".sorting");
sorting.removeAttribute("hidden");

let btnsShow = document.querySelectorAll(".trainer__show-more");
let modalScreen = document.querySelector("#modal-template");
let mainPage = document.querySelector(".page-main");
let body = document.querySelector(".body");

cardsContainer.addEventListener("click", function (event) {
  showModal(event);
  closeModal();
});

function showModal(event) {
  if (event.target.closest(".trainer__show-more")) {
    let photoCard = event.target.parentElement
      .querySelector(".trainer__img")
      .getAttribute("src");
    const modalData = DATA.find((item) => {
      return photoCard === item.photo;
    });
    fragment = modalScreen.content.cloneNode(true);
    fragment.querySelector(
      ".modal__name"
    ).textContent = `${modalData["last name"]} ${modalData["first name"]}`;
    fragment.querySelector(
      ".modal__point--category"
    ).textContent = `Категорія: ${modalData.category}`;
    fragment.querySelector(
      ".modal__point--experience"
    ).textContent = `Досвід: ${modalData.experience}`;
    fragment.querySelector(
      ".modal__point--specialization"
    ).textContent = `Напрям тренера: ${modalData.specialization}`;
    fragment.querySelector(
      ".modal__text"
    ).textContent = `${modalData.description}`;
    fragment.querySelector(".modal__img").src = modalData.photo;

    mainPage.append(fragment);
    body.style.overflow = "hidden";
  }
}

function closeModal() {
  let closeButton = document.querySelector(".modal__close");
  if (closeButton) {
    let modal = document.querySelector(".modal");
    closeButton.addEventListener("click", function () {
      modal.remove();
      body.style.overflow = "auto";
    });
  }
}

let btnSortUsual = document.querySelector(".sorting__element-usual");
let btnSortSurname = document.querySelector(".sorting__element-surname");
let btnSortExp = document.querySelector(".sorting__element-exp");
let sortBtns = document.querySelectorAll(".sorting__btn");
let sortSections = document.querySelector(".sorting");

let sortedTrainers = [...document.querySelectorAll(".trainer")];
let currentSort = DATA.slice();
let copyOfTrainers = sortedTrainers.map((value) => value);
sortSections.addEventListener("click", function (event) {
  sortData(event);
});

function activeBtn(event) {
  sortBtns.forEach((item) => {
    item.classList.remove("sorting__btn--active");
  });
  event.classList.add("sorting__btn--active");
}

function sortData(criteria) {
  switch (criteria.target.textContent.trim().toLowerCase()) {
    case "за замовчанням":
      fragment.append(...copyOfTrainers);
      cardsContainer.append(fragment);
      activeBtn(criteria.target);
      break;
    case "за прізвищем":
      sortedTrainers.sort((a, b) => {
        let contentA = a.querySelector(".trainer__name").textContent;
        let contentB = b.querySelector(".trainer__name").textContent;
        let LastNameA = contentA.split(" ")[0];
        let FirstNameA = contentA.split(" ")[1];
        let LastNameB = contentB.split(" ")[0];
        let FirstNameB = contentB.split(" ")[1];

        if (LastNameB === LastNameA) {
          return FirstNameA.localeCompare(FirstNameB);
        }
        return LastNameA.localeCompare(LastNameB);
      });
      fragment.append(...sortedTrainers);
      cardsContainer.append(fragment);
      activeBtn(criteria.target);
      break;
    case "за досвідом":
      sortedTrainers.forEach((item) => {
        let expValue = item.getAttribute("data-exp");
        expValue = expValue.slice(0, 2);
      });
      sortedTrainers.sort((a, b) => {
        let expValueA = parseInt(a.getAttribute("data-exp"));
        let expValueB = parseInt(b.getAttribute("data-exp"));
        return expValueB - expValueA;
      });
      fragment.append(...sortedTrainers);
      cardsContainer.append(fragment);
      activeBtn(criteria.target);
      break;
  }
}

let filters = document.querySelector(".sidebar__filters");
const input = [...document.querySelectorAll('[type="radio"]')];
filters.addEventListener("submit", filterData);

function filterData(event) {
  event.preventDefault();
  console.log(event);
  let mass = [];
  input.filter((elem) => {
    if (elem.checked) {
      mass.push(elem.labels[0].textContent.toLowerCase().trim());
    }
  });
  let spec = mass[0];
  let categ = mass[1];

  let filteredMass = DATA.filter((elem) => {
    if (
      (spec === "всі" || spec === elem.specialization.toLowerCase()) &&
      (categ === "всі" || categ === elem.category)
    ) {
      return true;
    }
  });
  sortedTrainers.forEach((elem) => {
    elem.style.display = "none";
  });
  sortedTrainers.filter((element) => {
    let photoCard = element.querySelector(".trainer__img").getAttribute("src");
    filteredMass.find((elem) => {
      if (elem.photo === photoCard) {
        element.style.display = "flex";
      }
    });
  });
}
