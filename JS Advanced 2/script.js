"use strict";
//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

//! 1.Коли ми хочемо обробити помилку і продовжувати використання коду.
//! 2.Коли ми хочемо використати змінну вийнятку.

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70,
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70,
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70,
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40,
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    },
];

function isValidBook(book) {
    return book.author && book.name && book.price;
}

function createBookList(books) {
    const ul = document.createElement("ul");

    books.forEach((book) => {
        if (isValidBook(book)) {
            const li = document.createElement("li");
            li.textContent = `Автор: ${book.author}, Назва: ${book.name}, Ціна: ${book.price}`;
            ul.appendChild(li);
        } else {
            const missingValue = [];
            if (!book.author) missingValue.push("author");
            if (!book.name) missingValue.push("name");
            if (!book.price) missingValue.push("price");
            console.error(
                `Помилка: Відсутня властивість(і): ${missingValue.join(", ")}`
            );
        }
    });

    return ul;
}

const root = document.getElementById("root");
if (root) {
    root.appendChild(createBookList(books));
}
