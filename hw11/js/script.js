"use strict";

/*1. Що таке події в JavaScript і для чого вони використовуються?
Подією є будь-яка дія, яку користувач може виконувати на сайті.Використовуються для змін на 
сайті відповідно до дій користувача.

2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
Що стосуються натисканя клавіш:
click, dbclick, mousedown, mouseup, contextmenu.
Що стосується переміщення курсору:
mouseenter, mouseLeave, mouseover, mouseout.
Та:
mousemove, select, wheel.

3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
Подія "contextmenu" - це подія, яка виникає, коли користувач викликає контекстне меню 
на елементі веб-сторінки при натисканні на праву кнопку миші.
*/

/*1. Додати новий абзац по кліку на кнопку:
По кліку на кнопку <button id="btn-click">Click Me</button>, 
створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">*/
let btnClick = document.getElementById("btn-click");
let contentSection = document.getElementById("content");
btnClick.addEventListener("click", function () {
    let newParagraph = document.createElement("p");
    newParagraph.textContent = "New Paragraph";
    contentSection.appendChild(newParagraph);
});

/*2. Додати новий елемент форми із атрибутами:
Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути,
 наприклад, type, placeholder, і name. та додайте його під кнопкою. */
let btnInputCreate = document.createElement("button");
btnInputCreate.setAttribute("id", "btn-input-create");
btnInputCreate.textContent = "Create Input";

let footer = document.querySelector("footer");
footer.parentNode.insertBefore(btnInputCreate, footer);
btnInputCreate.addEventListener("click", function () {
    let newInput = document.createElement("input");
    newInput.setAttribute("type", "text");
    newInput.setAttribute("placeholder", "Enter your text");
    newInput.setAttribute("name", "userInput");
    btnInputCreate.parentNode.insertBefore(
        newInput,
        btnInputCreate.nextSibling
    );
});
