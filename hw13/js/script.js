"use strict";

/*1. Яке призначення методу event.preventDefault() у JavaScript?
Щоб запобігати діям браузера.

2. В чому сенс прийому делегування подій?
Якщо у нас є багато елементів, то замість того, щоб призначати обробник для кожного з них,
ми встановлюємо один обробник на їхнього спільного предка.

3. Які ви знаєте основні події документу та вікна браузера?
Для документу:
DOMContentLoaded, load, beforeunload/unload.
Для браузера:
onload, onerror.
*/

/*Практичне завдання:
Реалізувати перемикання вкладок (таби) на чистому Javascript.
Технічні вимоги:
- У папці tabs лежить розмітка для вкладок. 
Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. 
При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися.
 При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
Умови:
- При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один). */
const tabs = document.querySelectorAll(".tabs-title");
const tabContent = document.querySelectorAll(".tabs-content li");

tabs.forEach((tab, index) => {
    tab.addEventListener("click", () => {
        removeActiveClass();
        tab.classList.add("active");

        hideAllTabs();
        tabContent[index].classList.add("active");
    });
});

function hideAllTabs() {
    tabContent.forEach((tab) => {
        tab.classList.remove("active");
    });
}

function removeActiveClass() {
    tabs.forEach((tab) => {
        tab.classList.remove("active");
    });
}
