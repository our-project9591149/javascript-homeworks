"use strict";

//? Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//! Коли в нас є об'єкт, до якого звертаються, шукаються його властивості, які потребуються.
//! Процес пошуку буде переходити в його прототип, у випадку якщо властивості не знайдуться.
//! Процес продовжуватиметься вздовж ієрархії прототипів до знайдення необхідної властивості аж до кінця ланцюга.
//? Для чого потрібно викликати super() у конструкторі класу-нащадка?
//! Щоб успадкувати батьківські властивості.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }

    set name(newName) {
        this._name = newName;
    }
    set age(newAge) {
        this._age = newAge;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }
    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }
}

const programmer1 = new Programmer("Олексій", 30, 1000, [
    "JavaScript",
    "Python",
]);
const programmer2 = new Programmer("Наталі", 25, 1200, ["Java", "C++"]);
const programmer3 = new Programmer("Роман", 28, 900, ["Ruby", "PHP"]);

console.log(
    `Ім'я: ${programmer1.name}, Вік: ${programmer1.age}, Зарплата: ${programmer1.salary}, Мови: ${programmer1.lang}`
);
console.log(
    `Ім'я: ${programmer2.name}, Вік: ${programmer2.age}, Зарплата: ${programmer2.salary}, Мови: ${programmer2.lang}`
);
console.log(
    `Ім'я: ${programmer3.name}, Вік: ${programmer3.age}, Зарплата: ${programmer3.salary}, Мови: ${programmer3.lang}`
);
