"use strict";
function fetchData(url) {
    return fetch(url)
        .then((response) => {
            if (!response.ok) {
                throw new Error(`Ошибка при загрузке данных с ${url}`);
            }
            return response.json();
        })
        .catch((error) => {
            console.error(error);
        });
}

class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
        this.element = this.createCardElement();
    }

    createCardElement() {
        const card = document.createElement("div");
        card.classList.add("card");

        card.innerHTML = `
            <h2>${this.post.title}</h2>
            <p>${this.post.body}</p>
            <p><strong>${this.user.name}</strong> (${this.user.email})</p>
            <button class="delete-button">Delete</button>
        `;

        card.querySelector(".delete-button").addEventListener("click", () =>
            this.deleteCard(card)
        );

        return card;
    }

    deleteCard(cardElement) {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: "DELETE",
        })
            .then((response) => {
                if (response.ok) {
                    cardElement.remove();
                } else {
                    alert("Не удалось удалить пост.");
                }
            })
            .catch((error) => {
                console.error("Ошибка:", error);
            });
    }

    render(container) {
        container.appendChild(this.element);
    }
}

function init() {
    const root = document.getElementById("root");

    Promise.all([
        fetchData("https://ajax.test-danit.com/api/json/users"),
        fetchData("https://ajax.test-danit.com/api/json/posts"),
    ])
        .then(([users, posts]) => {
            posts.forEach((post) => {
                const user = users.find((user) => user.id === post.userId);
                const card = new Card(post, user);
                card.render(root);
            });
        })
        .catch((error) => {
            console.log("Ошибка при загрузке данных:", error);
        });
}

document.addEventListener("DOMContentLoaded", init);
