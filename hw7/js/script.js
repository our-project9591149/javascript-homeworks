"use strict";
/*1. Як можна створити рядок у JavaScript?
Рядок можна створити у формі об'єкта.

2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
Між одинарними та подвійними різниця в тому, що в них використовується оператор конкатинації.
А всередені зворотніх лапок треба використовувати ${}.

3. Як перевірити, чи два рядки рівні між собою?
Можна в консолі їх порівняти за допомогою суворого дорівнює.

4. Що повертає Date.now()?
Він повертає поточні дату та час в мілісекундах від 1 січня 1970 року.

5. Чим відрізняється Date.now() від new Date()?
Date.now показує час в мілісекундах.
*/

/*1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome,
 яка приймає рядок str і повертає true, якщо рядок є паліндромом
(читається однаково зліва направо і справа наліво), або false в іншому випадку.


2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити,
 максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині,
  і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. 
  Приклади використання функції:
// Рядок коротше 20 символів
funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
funcName('checked string', 10); // false*/
function checkStringLength(str, maxLength) {
    return str.length <= maxLength;
}
console.log(checkStringLength("checked string", 20));
console.log(checkStringLength("checked string", 10));

/* 3. Створіть функцію, яка визначає скільки повних років користувачу.
Отримайте дату народження користувача через prompt. 
Функція повина повертати значення повних років на дату виклику функцію. */
