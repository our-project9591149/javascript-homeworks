"use strict";
//Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
//!AJAX це спосіб отримання даних з сервера.
function fetchFilms() {
    return fetch("https://ajax.test-danit.com/api/swapi/films")
        .then((response) => response.json())
        .catch((error) => {
            console.error("Error fetching films:", error);
        });
}

function fetchCharacters(characterUrls) {
    const characterPromises = characterUrls.map((url) =>
        fetch(url).then((response) => response.json())
    );
    return Promise.all(characterPromises).catch((error) => {
        console.error("Error fetching characters:", error);
    });
}

function displayFilm(film) {
    const filmContainer = document.createElement("div");
    filmContainer.className = "film";

    const title = document.createElement("h2");
    title.textContent = `Episode ${film.episodeId}: ${film.name}`;
    filmContainer.appendChild(title);

    const openingCrawl = document.createElement("p");
    openingCrawl.textContent = film.openingCrawl;
    filmContainer.appendChild(openingCrawl);

    const charactersContainer = document.createElement("div");
    charactersContainer.className = "characters";
    charactersContainer.innerHTML =
        '<p class="loading">Loading characters...</p>';
    filmContainer.appendChild(charactersContainer);

    document.getElementById("films").appendChild(filmContainer);

    fetchCharacters(film.characters).then((characters) => {
        charactersContainer.innerHTML = "";
        characters.forEach((character) => {
            const characterElement = document.createElement("p");
            characterElement.className = "character";
            characterElement.textContent = character.name;
            charactersContainer.appendChild(characterElement);
        });
    });
}

function displayFilms(films) {
    films.forEach((film) => {
        displayFilm(film);
    });
}

function init() {
    fetchFilms().then((data) => {
        if (Array.isArray(data)) {
            displayFilms(data);
        } else {
            console.error("Unexpected data format");
        }
    });
}

document.addEventListener("DOMContentLoaded", init);
